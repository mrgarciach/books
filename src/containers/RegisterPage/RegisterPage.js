import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Form, Button } from 'antd';
import RegisterContainer from './register.style'
import userActions from '../../redux/users/actions';

const { register, registerv2 } = userActions;
const FormItem = Form.Item;
class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false
    };
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, user) => {
      if (!err) {
        this.props.registerv2(user);
      }
    });
  }
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback("Las dos contraseñas son inconsistentes");
    } else {
      callback();
    }
  }
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['password_confirmation'], { force: true });
    }
    callback();
  }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  render() {
    // Pendiente MRGCH
    // const { registering } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div >
        <h2>Registro</h2>
        <RegisterContainer>
          <Form onSubmit={this.handleSubmit.bind(this)}>
            <Row gutter={48} type={"flex"} justify={"space-around"}>
              <Col xs={24} sm={24}>
                <FormItem id="formItem_FirstName" label={'Nombre'} hasFeedback >
                  {
                    getFieldDecorator('firstName', {
                      rules: [{
                        required: true,
                        message: 'El nombre es requerido'
                      }],
                      initialValue: ""
                    })(<Input name="firstName" id="input_firstName" />)
                  }
                </FormItem>
              </Col>
              <Col xs={24} sm={24}>
                <FormItem id="formItem_lastName" label={'Apellido'} hasFeedback  >
                  {
                    getFieldDecorator('lastName', {
                      rules: [{
                        required: true,
                        message: 'El apellido es requerido'
                      }],
                      initialValue: ""
                    })(<Input name="lastName" id="input_lastName" />)
                  }
                </FormItem>
              </Col>
              <Col xs={24} sm={24}>
                <FormItem id="formItem_UserName" label={'Nombre de usuario'} hasFeedback >
                  {
                    getFieldDecorator('username', {
                      rules: [{
                        required: true,
                        message: 'El nombre de usuario es requerido'
                      },
                      {
                        pattern: new RegExp(/^[a-z _-\d]+$/),
                        message: 'El formato es incorrecto'
                      }],
                      initialValue: ""
                    })(<Input name="username" id="input_username" />)
                  }
                </FormItem>
              </Col>
              <Col xs={24} sm={12}>
                <FormItem id="formItem_Password" label={'Contraseña'} hasFeedback>
                  {
                    getFieldDecorator('password', {
                      rules: [
                        { required: true, message: "La 'contraseña' es requerida" },
                        { min: 8, message: "La contraseña debe tener al menos 8 caracteres" },
                        { validator: this.validateToNextPassword }
                      ],
                      initialValue: "",
                    })(<Input type="password" name="password" id="input_Password" />)
                  }
                </FormItem>
              </Col>
              <Col xs={24} sm={12}>
                <FormItem id="formItemPasswordConfirm" label={"Confirmar contraseña"} hasFeedback>
                  {
                    getFieldDecorator('password_confirmation', {
                      rules: [
                        { required: true, message: "Confirmar Contraseña" },
                        { min: 8, message: "La contraseña debe tener al menos 8 caracteres" },
                        { validator: this.compareToFirstPassword, }
                      ],
                      initialValue: "",
                    })(<Input type="password"
                      name="password_confirmation"
                      id="input_passwordConfirmation"
                      onBlur={this.handleConfirmBlur} />)
                  }
                </FormItem>
              </Col>
              <Col>
                <Button
                  type="primary"
                  htmlType="submit"
                  id="button_editCreate" >
                  Registrarse
              </Button>
              </Col>
            </Row>
          </Form>
        </RegisterContainer>
      </div>
    );
  }
}

export default connect(
  state => ({
    registering: state.registration,
  }),
  { register, registerv2 }
)(Form.create()(RegisterPage));