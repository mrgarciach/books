import React, { Component } from 'react';
import { Tabs, Radio, Button, Modal } from 'antd';
import BooksTable from './books';
import UsersTable from './usersTable';
const TabPane = Tabs.TabPane;

class Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'left',
      visible: false 
    };
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  }
  handleOk = (e) => { 
    this.setState({
      visible: false,
    });
  }
  handleCancel = (e) => { 
    this.setState({
      visible: false,
    });
  }
  render() {
    const { mode } = this.state;
    let { items } = this.props.users;

    return (
      <div>
        <Modal
          title="Registrar nuevo libro"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          key={"modal-books"}
        >
        <p>Este modal será utilizado para agregar usuarios en el redux de la aplicación</p>
        </Modal>
        <Radio.Group onChange={this.handleModeChange} value={mode} style={{ marginBottom: 8 }}>
        </Radio.Group>
        <Tabs
          defaultActiveKey="users"
          tabPosition={mode}
        >
          <TabPane tab="Usuarios" key="users">
            <h5>Usuarios que se han registrado en la plataforma</h5> <br />
            <UsersTable users={items} />
          </TabPane>
          <TabPane tab="Libros" key="books">
            <h5>Libros que se han calificado en la plataforma</h5>
            <div className="button-action">
              <Button type="primary" onClick={this.showModal} >Agregar</Button>
            </div>
            <BooksTable books={this.props.books} />
          </TabPane>
        </Tabs>
      </div >
    );
  }
}

export default Information;


