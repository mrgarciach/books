import React from 'react';
import { Table } from 'antd';

function UsersTable({ users }) {

  const columns = [
    {
      title: 'Nombre',
      dataIndex: 'firstName',
      id: "firstName"
    },
    {
      title: 'Apellido',
      dataIndex: 'lastName',
      id: "lastName"
    },
    {
      title: 'Nombre de usuario',
      dataIndex: 'username',
      id: "columnUsername"
    }
  ];
  let data = users ? users : undefined;
  if (data) {
    data = data.filter(function (user) {
      user['key'] = user['username'];
      return user;
    });
  }
  return (
    <Table
      columns={columns}
      dataSource={data}
    />
  )
}
export default UsersTable;