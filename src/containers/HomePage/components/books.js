import React from 'react';
import { Card, Icon, Button } from 'antd';

const { Meta } = Card;

function BooksTable({
  books,
}) {
  return (
    <div>
      {
        books.map((book, index) => (
          <Card
            style={{ margin: '10px', }}
            key={ `card-${index}`}
          >
            <Meta
              title={book.title_book}
              description={book.author_name}
              key={ `meta-${index}`}
            />

            {(function (rows, i, len) {
              while (++i <= len) {
                rows.push(<Icon type="star" theme="filled"  key={ `icon-${i}`} />)
              }
              return rows;
            })([], 0, book.score)} 
          </Card>))
      }
    </ div >
  )
}
export default BooksTable;