import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import userActions from '../../redux/users/actions';
import Information from './components/Information'
import HomeContainer from './homepage.style'
const { getAll } = userActions;

class HomePage extends Component {

  componentDidMount() {
    this.props.getAll();
  }

  render() {
    const { user } = this.props;
    return (
      <HomeContainer>
        <Link to="/login">Logout</Link>
        <h1>¡Bienvenido {user && user.username}!</h1>
        <p> En este foro te mostramos una lista de libros recomendados para leer en cualquier época de tu vida porque son algunas de las mejores y más leídas obras de la historia.
            <br />
          <br />
          En mi opinión la lectura puede llegar a suponer un cambio real en nuestra  forma de ser y es evidente que influyen en las elecciones que tomamos, acciones que realizamos y logros que conseguimos. </p>
        <p>
        </p>
        <Information users={this.props.users} books={this.props.books}  />
      </HomeContainer>
    );
  }
}

export default connect(
  state => ({
    user: state.authentication.user,
    users: state.users,
    books: state.books.books
  }),
  { getAll }
)(HomePage);


