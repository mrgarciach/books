import styled from "styled-components";

const HomeContainer = styled.div`
a{
    display: flex;
    flex-flow: row-reverse;
 }
 .button-action{
    display: flex;
    justify-content: flex-end;
 }
`;
export default HomeContainer;
