import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        /*
         Si en el localstorage existe un usuario de muestra el contenido, 
         en caso contrario redirecciona al loggin
        */ 
        return (
            localStorage.getItem('user')
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
    }} />
)