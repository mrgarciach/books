import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../../helpers'
import { PrivateRoute } from './PrivateRoute';
import LoginPage from '../../containers/LoginPage';
import RegisterPage from '../../containers/RegisterPage';
import HomePage from '../HomePage';
import { AppContainer, Navbar, Footer } from './styles';
import { Icon } from 'antd';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    history.listen((location, action) => {
      // Pendiente MRGCH 
    });
  }
  render() {
    return (
      <AppContainer>
        <Navbar>
          <Icon width="50px" height="50px" className="icons" type="solution" />
        </Navbar>
        <div className="layoutContent">
          <Router history={history}>
            <div>
              <PrivateRoute exact path="/" component={HomePage} />
              <Route path="/login" component={LoginPage} />
              <Route path="/register" component={RegisterPage} />
            </div>
          </Router>
        </div>
        {!this.props.loggedIn &&
          <Footer>María Garcia-Chavez</Footer>
        }
      </AppContainer>
    );
  }
}

export default connect(
  state => ({
    loggedIn: state.authentication.loggedIn
  }),
)(App);


