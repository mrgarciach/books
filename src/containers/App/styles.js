import styled from "styled-components";

const AppContainer = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-flow: column;

.layoutContent {
  width: 70%;
  padding: 35px;
  background-color: #ffffff;
  border: 1px solid #ffffff;
  height: 100%;
}
`;

const Navbar = styled.div`
width: 100%;
height: 50px;
background-color: #373f47;
color:white;
.icons{
  padding: 15px;
  font-size: 28px;
  color:#ffffff;
}
}
`;

const Footer = styled.div`
display: flex;
justify-content: space-around;
width: 100%;
height: 100px;
background-color: #373f47;
position:absolute;
bottom: 0px;
color:white;
`;


export { AppContainer, Navbar, Footer };
