import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col, Input, Form, Button } from 'antd';
import userActions from '../../redux/users/actions';

const FormItem = Form.Item;
const { login, logout } = userActions;

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.props.logout();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, user) => {
      if (!err) {
        this.props.login(user.username, user.password)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h2>Login</h2>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Col xs={24} sm={24}>
            <FormItem id="formItem_UserName" label={'Nombre de usuario'} hasFeedback >
              {
                getFieldDecorator('username', {
                  rules: [{
                    required: true,
                    message: 'El nombre de usuario es requerido'
                  },],
                  initialValue: ""
                })(<Input name="username" id="input_username" />)
              }
            </FormItem>
          </Col>
          <Col xs={24} sm={24}>
            <FormItem id="formItem_Password" label={'Contraseña'} hasFeedback>
              {
                getFieldDecorator('password', {
                  rules: [
                    { required: true, message: "La 'contraseña' es requerida" },
                  ],
                  initialValue: "",
                })(<Input type="password" name="password" id="input_Password" />)
              }
            </FormItem>
          </Col>
          <Col>
            <Button
              type="primary"
              htmlType="submit"
              id="button_editCreate" >
              Ingresar
              </Button>
            <Link to="/register" style={{ marginLeft: '10px' }}>Registro</Link>
          </Col>
        </Form>
      </div>
    );
  }
}

export default connect(
  state => ({
    loggingIn: state.authentication
  }),
  { login, logout }
)(Form.create()(LoginPage));