import { combineReducers } from 'redux';
import authentication from './authentication/reducer';
import users from './users/reducer';
import books from './books/reducer'

const rootReducer = combineReducers({
  authentication,
  users,
  books
});

export default rootReducer;