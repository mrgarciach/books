import { all, call, put, takeEvery, fork } from 'redux-saga/effects';
import userActions from './actions';
import { history, authHeader } from '../../helpers';
import { notification } from 'antd';

function* logout() {
  yield takeEvery(userActions.USERS_LOGOUT, function* (action) {
    localStorage.removeItem('user');
  }
  );
}

function* login() {
  yield takeEvery(userActions.USER_LOGIN, function* (action) {
    const { username, password } = action;
    const rawResponse = yield call(login_user, username, password);

    if (rawResponse.ok) {
      let users;
      yield rawResponse.text().then(
        user => {
          // set in redux successull logged 
          users = JSON.parse(user);
          localStorage.setItem('user', user);
        }
      );
      if (users) {
        yield put({ type: userActions.USERS_LOGIN_SUCCESS, user: users });
        history.push('/');
      }
    } else {
      notification.error({
        message: rawResponse,
      });
    }
  }
  );
}

function* register() {
  yield takeEvery(userActions.USERS_REGISTER_REQUESTv2, function* (action) {

    const { user } = action;
    const rawResponse = yield call(register_user, user);
    if (rawResponse.ok) {
      history.push('/login');
      notification.success({
        message: 'Usuario registrado con éxito',
      })
    } else {
      notification.error({
        message: rawResponse,
      })
    }
  }
  );
}
function* getAll() {
  yield takeEvery(userActions.USERS_GETALL_REQUEST, function* (action) {
    const rawResponse = yield call(getall_user);
    if (rawResponse.ok) {
      let users;
      yield rawResponse.text().then(
        user => {
          // set in redux successull logged 
          users = JSON.parse(user);
        }
      );
      if (users) {
        yield put({ type: userActions.USERS_GETALL_SUCCESS, users });
      }
    }
  });
}

// Función auxiliar para el login
function login_user(username, password) {
  return new Promise((resolve, reject) => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password })
    };
    fetch(
      `/users/authenticate`,
      requestOptions,
    ).then((response) => {
      resolve(response)
    }
    ).catch((error) => {
      resolve(error)
    })
  });
}
// Función auxiliar para el registro
function register_user(user) {
  return new Promise((resolve, reject) => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ user })
    };
    fetch(
      `/users/register`,
      requestOptions,
    ).then((response) => {
      resolve(response)
    }
    ).catch((error) => {
      resolve(error)
    })
  });
}

// Funcion auxiliar para obtener todos los usuariosd
function getall_user() {
  return new Promise((resolve, reject) => {
    const requestOptions = {
      method: 'GET',
      headers: authHeader(),
    };
    fetch(
      `/users`,
      requestOptions,
    ).then((response) => {
      resolve(response)
    }
    ).catch((error) => {
      resolve(error)
    })
  });
}

export default function* rootSaga() {
  yield all([
    fork(login),
    fork(logout),
    fork(register),
    fork(getAll)
  ]);
}

