import actions from './actions';

export default (state = {}, action) => {
  switch (action.type) {
    // Request users
    case actions.USERS_GETALL_REQUEST:
      return {
        loading: true
      };
    case actions.USERS_GETALL_SUCCESS:
      return {
        items: action.users
      };
    case actions.USERS_GETALL_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}