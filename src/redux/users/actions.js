import { history } from '../../helpers';

const userActions = {

  USER_LOGIN: 'USER_LOGIN',
  USERS_LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
  USERS_LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
  USERS_LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
  /**
   *  This action is used for log into the app
   *  @param {String} username 
   *  @param {string} password
   */
  login: (username, password) => ({
    type: userActions.USER_LOGIN,
    username,
    password,
  }),

  USERS_LOGOUT: 'USERS_LOGOUT',
  /**
   *  This action is used for logout the app
   *  No params used
   */
  logout: () => ({
    type: userActions.USERS_LOGOUT
  }),
  USERS_REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
  USERS_REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',
  /**
   *  This action is used for register new users
   *   @param {object} user 
   */
  USERS_REGISTER_REQUESTv2: 'USERS_REGISTER_REQUESTv2',
  registerv2: (user) => ({
    type: userActions.USERS_REGISTER_REQUESTv2,
    user,
  }),

  USERS_GETALL_REQUEST: 'USERS_GETALL_REQUEST',
  USERS_GETALL_SUCCESS: 'USERS_GETALL_SUCCESS',
  USERS_GETALL_FAILURE: 'USERS_GETALL_FAILURE',
  /**
   *  This action is used for get all registered users
   *  No params used
   */
  getAll: () => ({
    type: userActions.USERS_GETALL_REQUEST,
  }),
};
export default userActions;
