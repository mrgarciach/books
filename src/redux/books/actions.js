const booksActions = {
  BOOK_ADD_REQUEST: 'BOOK_ADD_REQUEST',
  /**
   *  This action is used for add a new BOOK
   */
  addBook: () => ({
    type: booksActions.BOOK_ADD_REQUEST,
  }),
};
export default booksActions;
