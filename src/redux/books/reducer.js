import actions from './actions';
const books_base = [
    {
        author_name: 'A. Dumas',
        title_book: 'El conde de montecristo',
        score: 5
    },
    {
        author_name: 'F. Dostoievski',
        title_book: 'Crimen y casstigo',
        score: 2
    },
    {
        author_name: 'A. Dumas',
        title_book: 'Los tres mosqueteros',
        score: 1
    },
    {
        author_name: 'S. Larsson,',
        title_book: 'Los hombres que no amaban a las mujeres',
        score: 5
    },

]
export default (state = { books: books_base }, action) => {
    switch (action.type) {
        // Request users
        case actions.BOOK_ADD_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}