import userAction from '../../redux/users/actions'
let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export default (state = initialState, action) => {
  switch (action.type) {
    // Loginn
    case userAction.USERS_LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userAction.USERS_LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userAction.USERS_LOGIN_FAILURE:
      return {};
    case userAction.USERS_USERS_LOGOUT:
      return {};
    // Register
    case userAction.USERS_REGISTER_REQUEST:
      return { registering: true };
    case userAction.USERS_REGISTER_SUCCESS:
      return {};
    case userAction.USERS_REGISTER_FAILURE:
      return {};
    default:
      return state
  }
}